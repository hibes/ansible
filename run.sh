#/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

main() {
  git -C ${THIS_DIR} pull

  ansible-playbook -i ${THIS_DIR}/inventory ${THIS_DIR}/site.yml
}

main "$@"
