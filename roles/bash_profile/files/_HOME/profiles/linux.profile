#!/bin/bash

if [ ! -z "$(which wmctrl)" ]; then
  alias tl="tl1"
  alias bl="bl1"
  alias tr="tr1"
  alias br="br1"
  alias tl1="wmctrl -r :ACTIVE: -e \"0,0,0,960,511\""
  alias bl1="wmctrl -r :ACTIVE: -e \"0,0,550,960,511\""
  alias tr1="wmctrl -r :ACTIVE: -e \"0,960,0,960,511\""
  alias br1="wmctrl -r :ACTIVE: -e \"0,960,550,960,511\""
  alias tl2="wmctrl -r :ACTIVE: -e \"0,1920,0,960,511\""
  alias bl2="wmctrl -r :ACTIVE: -e \"0,1920,550,960,511\""
  alias tr2="wmctrl -r :ACTIVE: -e \"0,2880,0,960,511\""
  alias br2="wmctrl -r :ACTIVE: -e \"0,2880,550,960,511\""
fi
