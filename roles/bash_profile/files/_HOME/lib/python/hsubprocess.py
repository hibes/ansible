#!/usr/bin/env python

import subprocess
import sys
import select

def call(args, silence=False):
  p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  stdout = []
  stderr = []

  while True:
    reads = [p.stdout.fileno(), p.stderr.fileno()]
    ret = select.select(reads, [], [])

    for fd in ret[0]:
      if fd == p.stdout.fileno():
        read = p.stdout.readline()

        if (len(read) > 0):
          if (silence == False):
            sys.stdout.write(read)
          stdout.append(read)
      if fd == p.stderr.fileno():
        read = p.stderr.readline()

        if (len(read) > 0):
          if (silence == False):
            sys.stderr.write(read)
          stderr.append(read)

    if p.poll() != None:
      return {
        "returncode": p.returncode,
        "stdout": stdout,
        "stderr": stderr,
        "args": args
        }

def check_output(args):
  if (isinstance(args, basestring)):
    process = subprocess.Popen(shlex.split(args), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  else:
    process = subprocess.Popen(args, stdout=subprocess.STDOUT, stderr=subprocess.STDOUT)

