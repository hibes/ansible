#!/usr/bin/env python

import json

def pp(obj):
  return json.dumps(obj, indent=2)
