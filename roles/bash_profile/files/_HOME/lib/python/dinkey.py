#!/usr/bin/env python

import hsubprocess
import json
import os
import random
import requests
import time

INIT_SLEEP        = 2
INC_SLEEP         = 2
ROUNDTRIP_TIMEOUT = 20

def dinkey_roundtrip(host, inputFile, outputFile):
  dinkey_up_result = dinkey_up(host, inputFile)

  time.sleep(INIT_SLEEP)
  elapsed = INIT_SLEEP

  id = dinkey_up_result["id"]

  url = "http://%s:3000/getFile/%s" % (host, id)

  while (True):
    response = requests.head(url)

    if (response.status_code == 200):
      break;
    elif (response.status_code == 500):
      print("""
      There was an error on the server -- usually this means the binary
      isn't protected or has already been protected, check that you run DD_Prot_Check
      and rebuild, then try again.
      """)

      return response.status_code
    elif (response.status_code == 404):
      print("The server ran into an error retrieving the file")

      return response.status_code

    time.sleep(INC_SLEEP)
    elapsed += INC_SLEEP

    if (elapsed > ROUNDTRIP_TIMEOUT):
        print("Failed to retrieve file from dinkey server...")
        print(response)

        return 404

  result = dinkey_down(host, dinkey_up_result["id"], outputFile)
  print("Got file %s" % result["output"])

  return 0

def dinkey_down(host, id, outputFile):
  return {
    "returncode": hsubprocess.call([ "wget", "http://%s:3000/getFile/%s" % (host, id), "-O", "%s" % outputFile], silence=True)["returncode"],
    "output": outputFile
  }

def dinkey_up(host, pathToFile):
  filename = os.path.basename(pathToFile)
  id = random.randrange(0, 999999999)

  response = requests.put(
    "http://%s:3000/%s/%d" % (host, filename, id),
    data=open(pathToFile).read(),
    headers={"content-type": "application/octet-stream"}
    )

  if (response.status_code == requests.codes.ok):
    rc = 0
  else:
    rc = response.status_code

  return {
    "id": id,
    "filename": response.json()["filename"],
    "returncode": rc
    }

  return {
    "id": id,
    "filename": filename,
    "returncode": hsubprocess.call([ "curl", "-T", pathToFile, "http://%s:3000/%s/%d" % (host, filename, id) ])["returncode"]
    }

