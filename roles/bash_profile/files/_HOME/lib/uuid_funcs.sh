#!/bin/bash

is_uuid() {
  if [[ "$1" =~ ^[a-f0-9]{8}[-][a-f0-9]{4}[-][a-f0-9]{4}[-][a-f0-9]{4}[-][a-f0-9]{12}$ ]]; then
    return 1
  fi

  return 0
}

get_uuid() {
  echo $1 | grep -oE "[a-f0-9]{8}[-][a-f0-9]{4}[-][a-f0-9]{4}[-][a-f0-9]{4}[-][a-f0-9]{12}"
}

