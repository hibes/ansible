#!/bin/bash

umask 002
uname="$(uname -o 2>&1)"

if [ $? -ne 0 ]; then
  uname="$(uname)"
fi

profile_src="$HOME/profiles/default.profile"

if [[ "$uname" =~ "Cygwin" ]]; then
  profile_src="$HOME/profiles/cygwin.profile"
elif [[ "$uname" =~ "Linux" ]]; then
  profile_src="$HOME/profiles/linux.profile"
elif [[ "$uname" =~ "Darwin" ]]; then
  profile_src="$HOME/profiles/darwin.profile"
fi

if [ -e "$profile_src" ]; then
  . $profile_src
fi

if [ ! -f "$HOME/.vimrc" ]; then
  if [ -f "$HOME/cli_confs/.vimrc" ]; then
    ln -sf $HOME/cli_confs/.vimrc $HOME/.vimrc
  fi
fi

export EDITOR=vi

