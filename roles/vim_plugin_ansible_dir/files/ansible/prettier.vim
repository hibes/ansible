if has("autocmd")
  call plug#begin('~/.vim/plugged')
    Plug 'prettier/vim-prettier', { 'do': 'yarn install', 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown'] }
  call plug#end()

  " Add the following line (uncommented) to ~/.vimrc to disable this
  "let g:prettierAllDisabled = 1
  function s:PrettierIfNotDisabled()
    if !get(g:, 'prettierAllDisabled', 0)
      Prettier
    endif
  endf

  augroup prettier
    autocmd BufWritePre *.js call s:PrettierIfNotDisabled()
  augroup END
endif
