silent !mkdir -p ~/.vim/tmp >/dev/null 2>&1
silent !mkdir -p ~/.vim/tmp/bkpdir >/dev/null 2>&1
silent !mkdir -p ~/.vim/tmp/swapdir >/dev/null 2>&1
silent !mkdir -p ~/.vim/tmp/undodir >/dev/null 2>&1

set backup
set backupdir=~/.vim/tmp/bkpdir//
set dir=~/.vim/tmp/swapdir//
set undodir=~/.vim/tmp/undodir//
