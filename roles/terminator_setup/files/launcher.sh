#!/bin/bash

layout=$1

terminator -l $layout --geometry $(wmctrl -d | grep -oE "[0-9]+x[0-9]+" | tail -n 1)
