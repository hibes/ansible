#!/usr/bin/python

import serial
import subprocess
import time

def main():
    lsusb_output = subprocess.check_output('lsusb', shell=True).split('\n')

    for dev in subprocess.check_output("ls /dev/ttyUSB* /dev/ttyACM* 2> /dev/null || return 0", shell=True).split('\n'):
        if (dev != ""):
            bn = subprocess.check_output("basename %s" % dev, shell=True).split('\n')[0]
            deep = subprocess.check_output("find /sys/devices -name %s | head -n 1" % bn, shell=True).split('\n')[0]

            idp = subprocess.check_output('cat $(find %s/../.. -name idProduct)' % deep, shell=True).split('\n')[0]
            idv = subprocess.check_output('cat $(find %s/../.. -name idVendor)' % deep, shell=True).split('\n')[0]

            for line in lsusb_output:
                if (idp in line and idv in line):
                    print("%s: %s" % (dev, line))

                    peek(dev)

def isOccupied(device):
    """
    TODO: More checking to see if device might be in use.  checking for a minicom lock file?
    """
    results = subprocess.check_output('ps -ax | grep -E "minico[m].*%s" || return 0' % device, shell=True)
    if (len(results) > 1):
        return True

    return False


def peek(device):
    if (isOccupied(device)):
        print("Couldn't peek, device in use")
    else:
        ser = serial.Serial(device, 115200, timeout=0.5)
        ser.write("help\r\n")
        time.sleep(0.1)
        response = ser.read(size=255)
        ser.close()

        print(response)

main()
